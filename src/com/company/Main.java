package com.company;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static final String KOI8_U = "KOI8-U";

    public static void main(String[] args) {
        String jaString = new String("Скопіюйте текст для розкодування у велике поле для тексту. Програма спробуе розшифрувати перших кілька слів, тому вони мусять бути текстом в кирилиці (не латинка, не дата і не число).");
        writeOutput(jaString, KOI8_U);
        String inputString = readInput(KOI8_U);
        System.out.println(inputString);
        writeOutput(inputString, "UTF-8");
        inputString = readInput("UTF-8");
        System.out.println(inputString);

    }

    private static String readInput(String encoding) {
        StringBuffer buffer = new StringBuffer();
        try {
            FileInputStream fis = new FileInputStream("test.txt");
            InputStreamReader isr = new InputStreamReader(fis, "KOI8-U");
            Reader in = new BufferedReader(isr);
            int ch;
            while ((ch = in.read()) > -1) {
                buffer.append((char)ch);
            }
            in.close();
            return buffer.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void writeOutput(String str, String encoding) {
        try {
            FileOutputStream fos = new FileOutputStream("test.txt");
            Writer out = new OutputStreamWriter(fos, encoding);
            out.write(str);
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
